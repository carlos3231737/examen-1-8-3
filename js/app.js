const Fetch =()=>{
    const titulo = document.getElementById("titulo").value.trim()
    const url = `https://www.omdbapi.com/?t=${titulo}&plot=full&apikey=30063268`
    fetch(url)
    .then(respuesta=>respuesta.json())
    .then(data=> mostrar(data, titulo))
    .catch(reject=>{
        console.log("surgio un error" +reject)
    })
}

const mostrar=(data, titulo)=>{
    if (!data || data.Response === "False" || !(data.Title.toLowerCase() === titulo.toLowerCase())) {
        alert("Película no encontrada o error en la solicitud.");
        return;
    }
    let nom = document.getElementById("nom")
    let sal = document.getElementById("sal")
    let act = document.getElementById("act")
    let img = document.getElementById("pel")
    let res = document.getElementById("res")
    img.setAttribute("src", data.Poster)
    nom.innerHTML = "<b>Nombre: </b>" +data.Title
    sal.innerHTML = "<b>Año de Realización: </b>" +data.Year
    act.innerHTML = "<b>Actores principales: </b>" +data.Actors
    res.innerHTML = data.Plot
}

document.getElementById("btnBus").addEventListener("click", Fetch);